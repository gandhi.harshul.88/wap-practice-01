package com.practice.backend.services;

import com.practice.backend.datamodel.dto.UserAccount;

public interface UserAccountService {
	public UserAccount getUserAccountByUserName(String userName);
	public void saveUserCredentials(UserAccount userAccount);
	public Boolean verifyLoginDetails(String userName, String password);
	public String getUserPassword(String username);
	public Boolean verifyPassword(String username, String password);
}
