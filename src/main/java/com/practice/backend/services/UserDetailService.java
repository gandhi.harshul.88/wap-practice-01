package com.practice.backend.services;

import com.practice.backend.datamodel.dto.UserDetails;

public interface UserDetailService {
	public int getUserId(String name, String email, String telephone);
	public void saveUserDetail(UserDetails userDetail);
}
