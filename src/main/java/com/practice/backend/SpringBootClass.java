package com.practice.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootClass {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootClass.class);
	}
}
