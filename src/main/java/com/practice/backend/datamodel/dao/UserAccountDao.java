package com.practice.backend.datamodel.dao;

import com.practice.backend.datamodel.dto.UserAccount;

public interface UserAccountDao {
	public void saveOrUpdateLoginCredentials(UserAccount loginCred);
	public UserAccount getLoginCredentials(String userName);
}
