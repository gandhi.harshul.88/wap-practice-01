package com.practice.backend.datamodel.dao;

import java.util.List;

import com.practice.backend.datamodel.dto.UserDetails;


public interface UserDetailDao {
	public void saveOrUpdateUser(UserDetails user);
	public void delete(String user_id);
	public UserDetails getUser(String user_id);
	public List<UserDetails> list();
	public UserDetails getUserIdByAllFields(String name, String email, String telephone);
}
