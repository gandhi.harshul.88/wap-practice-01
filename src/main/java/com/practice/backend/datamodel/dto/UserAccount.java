package com.practice.backend.datamodel.dto;

public class UserAccount {
	private int userId;
	private String userName;
	private String password;
	
	public UserAccount(){}
	
	public UserAccount(int userId, String userName, String password){
		this.userId = userId;
		this.userName = userName;
		this.password = password;
	}
	
	public void setUserId(int userId){
		this.userId = userId;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setPassword(String password){
		this.password = password;
	}
	
	public int getUserId(){
		return userId;
	}
	public String getUserName(){
		return userName;
	}
	public String getPassword(){
		return password;
	}
}
