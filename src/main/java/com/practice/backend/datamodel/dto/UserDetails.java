package com.practice.backend.datamodel.dto;

public class UserDetails {
	private int userId;
	private String name;
	private String email;
	private String telephone;

	public UserDetails(){}
	
	public UserDetails(String name, String email, String telephone){
		this.name = name;
		this.email = email;
		this.telephone = telephone;
	}
	
	public UserDetails(int userId, String name, String email, String telephone){
		this.userId = userId;
		this.name = name;
		this.email = email;
		this.telephone = telephone;
	}
	
	public void setUserId(int userId){
		this.userId = userId;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setEmail(String email){
		this.email  = email;
	}
	public void setTelephone(String telephone){
		this.telephone = telephone;
	}
	
	public int getUserId(){
		return this.userId;
	}
	public String getName(){
		return this.name;
	}
	public String getEmail(){
		return this.email;
	}
	public String getTelephone(){
		return this.telephone;
	}
}
