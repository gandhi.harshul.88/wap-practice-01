package com.practice.backend.datamodel.daoImpl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.practice.backend.datamodel.dao.UserDetailDao;
import com.practice.backend.datamodel.dto.UserAccount;
import com.practice.backend.datamodel.dto.UserDetails;

@Repository
public class UserDetailDaoImpl implements UserDetailDao {
	
	private JdbcTemplate jdbcTemplate;
	public UserDetailDaoImpl(){}
	
	@Autowired
	public UserDetailDaoImpl(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void saveOrUpdateUser(UserDetails user) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO user (name, email, telephone) "
				+ "VALUES (?,?,?)";
		jdbcTemplate.update(sql, user.getName(), user.getEmail(), user.getTelephone());
	}

	
	@Override
	public List<UserDetails> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String user_id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM user WHERE userId = ?";
		jdbcTemplate.update(sql, user_id);
		
	}

	@Override
	public UserDetails getUser(String user_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDetails getUserIdByAllFields(String name, String email, String telephone) {
		// TODO Auto-generated method stub
		String q = "SELECT userId from user WHERE name = ? and email = ? and telephone = ?";
				try{
			return jdbcTemplate.queryForObject(
					q, (rs, rownum) -> {
						return new UserDetails(rs.getInt("userId"), name, email, telephone);}
					, name, email, telephone);
		}catch(DataAccessException e){
			e.printStackTrace();
		}
		return null;
	}
}
