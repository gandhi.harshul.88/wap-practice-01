package com.practice.backend.datamodel.daoImpl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.practice.backend.datamodel.dao.UserAccountDao;
import com.practice.backend.datamodel.dto.UserAccount;

@Repository
public class UserAccountDaoImpl implements UserAccountDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public UserAccountDaoImpl(){}
	
	@Autowired
	public UserAccountDaoImpl(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void saveOrUpdateLoginCredentials(UserAccount loginCred) {
		// TODO Auto-generated method stub
		String q = "INSERT INTO login (userId, userName, password)"
				+ " VALUES (?,?,?)";
		jdbcTemplate.update(q, loginCred.getUserId(), loginCred.getUserName(), loginCred.getPassword());;
	}

	@Override
	public UserAccount getLoginCredentials(String userName) {
		// TODO Auto-generated method stub
		String q = "SELECT userId, userName, password from login where username = ?";
		try{
			return jdbcTemplate.queryForObject(
					q, (rs, rownum) -> {
						return new UserAccount(rs.getInt("userId"), rs.getString("userName"), rs.getString("password"));}
					, userName);
		}catch(DataAccessException e){
			e.printStackTrace();
		}
		return null;
	}
}
