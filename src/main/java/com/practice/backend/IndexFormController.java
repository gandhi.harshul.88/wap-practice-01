package com.practice.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.practice.backend.datamodel.dao.UserDetailDao;
import com.practice.backend.datamodel.dto.UserAccount;
import com.practice.backend.datamodel.dto.UserDetails;
import com.practice.backend.services.UserAccountService;
import com.practice.backend.services.UserDetailService;
import com.practice.frontend.LoginForm;
import com.practice.frontend.SignupForm;
import com.practice.frontend.UserDetailForm;

@Controller
public class IndexFormController {
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private UserDetailDao userDao;
	
	int userId;
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String indexPage(){
		return "index";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String loginPage(Model model){
		model.addAttribute("loginFormObj", new LoginForm());
		return "login";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public String userDetailForm(Model model){
		model.addAttribute("userDetailsObj", new UserDetailForm());
		return "signup";
	}
	
	
	@RequestMapping(value="/saveUserDetails", method=RequestMethod.POST)
	public String userPage(@ModelAttribute UserDetailForm userDetailsObj, Model model){	
		model.addAttribute("userDetailsObj", userDetailsObj);
		System.out.println("Captured user details\nName : "+userDetailsObj.getName()+
				"\nEmail : "+userDetailsObj.getEmail()+
				"\nTelephone : "+userDetailsObj.getTelephone());
		userDetailService.saveUserDetail(new UserDetails(userDetailsObj.getName(), userDetailsObj.getEmail(), userDetailsObj.getTelephone()));
		userId = userDetailService.getUserId(userDetailsObj.getName(), userDetailsObj.getEmail(), userDetailsObj.getTelephone());
		return "user";
	}
	
	@RequestMapping(value="/signUpForm", method=RequestMethod.GET)
	public String attachObjectToSignUpForm(Model model){
		model.addAttribute("signUpFormObj", new SignupForm());
		return "signupForm";
	}
	
	@RequestMapping(value="/saveCredentials", method=RequestMethod.POST)
	public String saveUserCredentials(@ModelAttribute SignupForm signUpFormObj, Model model){	
		model.addAttribute("signUpFormObj", signUpFormObj);
		System.out.println("Captured credentials\nUserId : "+userId
				+ "\nUserName : "+signUpFormObj.getUsername()+
				"\nPassword : "+signUpFormObj.getPassword());
		userAccountService.saveUserCredentials(new UserAccount(userId, signUpFormObj.getUsername(), signUpFormObj.getPassword()));
		
		return "user";
	}
	
	@RequestMapping(value="/verifyUser", method=RequestMethod.POST)
	public String userPage(@ModelAttribute LoginForm loginFormObj, Model model){	//After login
		model.addAttribute("loginFormObj", loginFormObj);
		System.out.println("Credentials intercepted [Username : "+loginFormObj.getUsername()+"] and [Password : "+loginFormObj.getPassword()+"]\n");
		if(userAccountService.verifyPassword(loginFormObj.getUsername(), loginFormObj.getPassword())){
			return "user";
		}else{
			return "loginFailed";
		}
	}
	
	
	
}