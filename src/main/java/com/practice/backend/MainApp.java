package com.practice.backend;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MainApp {
	
	@Scheduled(fixedRate=3000)
	public void printWelcomeMessage(){
		System.out.println(" **** PRACTICE SPRING APPLICATION IS UP **** ");
	}
}
