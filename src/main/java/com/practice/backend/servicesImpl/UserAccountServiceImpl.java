package com.practice.backend.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.backend.datamodel.dao.UserAccountDao;
import com.practice.backend.datamodel.dto.UserAccount;
import com.practice.backend.services.UserAccountService;

@Service
public class UserAccountServiceImpl implements UserAccountService {
	
	@Autowired
	private UserAccountDao userAccountDao;

	@Override
	public UserAccount getUserAccountByUserName(String userName) {
		// TODO Auto-generated method stub
		return userAccountDao.getLoginCredentials(userName);
	}


	@Override
	public Boolean verifyLoginDetails(String userName, String password) {
		// TODO Auto-generated method stub
		UserAccount userAccount = getUserAccountByUserName(userName);
		return userAccount.getPassword().equals(password);
	}


	@Override
	public void saveUserCredentials(UserAccount userAccount) {
		// TODO Auto-generated method stub
		userAccountDao.saveOrUpdateLoginCredentials(userAccount);
	}


	@Override
	public String getUserPassword(String username) {
		// TODO Auto-generated method stub
		UserAccount userAccount = userAccountDao.getLoginCredentials(username);
		return userAccount.getPassword();
	}


	@Override
	public Boolean verifyPassword(String username, String password) {
		// TODO Auto-generated method stub
		return getUserPassword(username).equals(password);
	}
	
	

}
