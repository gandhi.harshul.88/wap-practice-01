package com.practice.backend.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.backend.datamodel.dao.UserDetailDao;
import com.practice.backend.datamodel.dto.UserDetails;
import com.practice.backend.services.UserDetailService;

@Service
public class UserDetailServiceImpl implements UserDetailService{

	@Autowired
	private UserDetailDao userDetailDao;
	
	@Override
	public int getUserId(String name, String email, String telephone) {
		// TODO Auto-generated method stub
		UserDetails userDetail = userDetailDao.getUserIdByAllFields(name, email, telephone);
		System.out.println("User Id received for name: "+name+", email: "+email+" and telephone: "+telephone+" is ==> "+userDetail.getUserId());
		return userDetail.getUserId();
	}

	@Override
	public void saveUserDetail(UserDetails userDetail) {
		// TODO Auto-generated method stub
		userDetailDao.saveOrUpdateUser(userDetail);
	}

}
