package com.practice.frontend;

public class UserDetailForm {
	private String userId;
	private String name;
	private String email;
	private String telephone;

	public UserDetailForm(){}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setEmail(String email){
		this.email  = email;
	}
	public void setTelephone(String telephone){
		this.telephone = telephone;
	}
	
	public String getUserId(){
		return this.userId;
	}
	public String getName(){
		return this.name;
	}
	public String getEmail(){
		return this.email;
	}
	public String getTelephone(){
		return this.telephone;
	}
}
